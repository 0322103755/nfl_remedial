from django.contrib import admin
from .models import Team, Ciudad, Estadio
# Register your models here.

@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = [
        "id_team",
        "name",
        "city",
        "stadium"
    ]

@admin.register(Ciudad)
class CiudadAdmin(admin.ModelAdmin):
    list_display = [
        "id_ciudad",
        "name"
    ]

@admin.register(Estadio)
class EstadioAdmin(admin.ModelAdmin):
    list_display = [
        "id_estadio",
        "name"
    ]