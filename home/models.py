from django.db import models

class Estadio(models.Model):
    id_estadio = models.AutoField(primary_key=True)
    name = models.CharField(max_length=80)

    def __str__(self):
        return self.name
    
class Ciudad(models.Model):
    id_ciudad = models.AutoField(primary_key=True)
    name = models.CharField(max_length=80)

    def __str__(self):
        return self.name

class Team(models.Model):
    id_team = models.AutoField(primary_key=True)
    name = models.CharField(max_length=80)
    city = models.ForeignKey(Ciudad, on_delete=models.CASCADE, null=True)
    stadium = models.ForeignKey(Estadio, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name

