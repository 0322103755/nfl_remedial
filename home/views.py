from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.views import generic
from django.urls import reverse_lazy
from .models import Team, Ciudad, Estadio
from .forms import UpdateTeamForm, NewTeamForm, UpdateCiudadForm, NewCiudadForm, UpdateEstadioForm, NewEstadioForm

#VIEW EQUIPO

#DETAIL
class DetailTeam(generic.DetailView):
    template_name = "home/teams/detail_equipo.html"
    context = {}

    def get(self, request, pk):
        self.context = {
            "equipo": Team.objects.get(pk=pk)
        }
        return render(request, self.template_name, self.context)


#DELETE
class DeleteTeam(generic.DeleteView):
    template_name = "home/teams/delete_equipo.html"
    model = Team
    success_url = reverse_lazy("home:equipoindex") 


#CREATE
class NewTeam(generic.CreateView):
    template_name = 'home/teams/new_equipo.html'
    model = Team
    form_class = NewTeamForm
    success_url = reverse_lazy('home:teamindex')


#UPDATE
class UpdateTeam(generic.UpdateView):
    template_name = 'home/teams/update_equipo.html'
    model = Team
    form_class = UpdateTeamForm
    success_url = reverse_lazy("home:teamindex")


#PLANTILLA HOME/TEAMINDEX
class TeamIndex(generic.View):
    template_name = "home/teams/teamindex.html"
    context = {}

    def get(self, request):
        self.context = {
            "equipos": Team.objects.all()
        }
        return render(request, self.template_name, self.context)


class EquipoIndex(generic.View):
    template_name = "base/equipoindex.html"
    context = {}

    def get(self, request):
        return render(request, self.template_name, self.context)


class Index(generic.View):
    template_name = "base/lobby.html"
    context = {}

    def get(self, request):
        return render(request, self.template_name, self.context)


#CIUDADES

class DetailCiudad(generic.DetailView):
    template_name = "home/cities/detail_ciudad.html"
    context = {}

    def get(self, request, pk):
        self.context = {
            "ciudad": Ciudad.objects.get(pk=pk)
        }
        return render(request, self.template_name, self.context)


class DeleteCiudad(generic.DeleteView):
    template_name = "home/cities/delete_ciudad.html"
    model = Ciudad
    success_url = reverse_lazy("home:ciudadindex") 

class NewCiudad(generic.CreateView):
    template_name = 'home/cities/new_ciudad.html'
    model = Ciudad
    form_class = NewCiudadForm
    success_url = reverse_lazy('home:ciudadindex')


class UpdateCiudad(generic.UpdateView):
    template_name = 'home/cities/update_ciudad.html'
    model = Ciudad
    form_class = UpdateCiudadForm
    success_url = reverse_lazy("home:ciudadindex")


class CityIndex(generic.View):
    template_name = "home/cities/cityindex.html"
    context = {}

    def get(self, request):
        self.context = {
            "ciudads": Ciudad.objects.all()
        }
        return render(request, self.template_name, self.context)

#ESTADIOS

class DetailEstadio(generic.DetailView):
    template_name = "home/stadiums/detail_estadio.html"
    context = {}

    def get(self, request, pk):
        self.context = {
            "estadio": Estadio.objects.get(pk=pk)
        }
        return render(request, self.template_name, self.context)


class DeleteEstadio(generic.DeleteView):
    template_name = "home/stadiums/delete_estadio.html"
    model = Estadio
    success_url = reverse_lazy("home:estadioindex") 


class NewEstadio(generic.CreateView):
    template_name = 'home/stadiums/new_estadio.html'
    model = Estadio
    form_class = NewEstadioForm
    success_url = reverse_lazy('home:estadioindex')


class UpdateEstadio(generic.UpdateView):
    template_name = 'home/stadiums/update_estadio.html'
    model = Estadio
    form_class = UpdateEstadioForm
    success_url = reverse_lazy("home:estadioindex")


class StadiumIndex(generic.View):
    template_name = "home/stadiums/stadiumindex.html"
    context = {}

    def get(self, request):
        self.context = {
            "estadios": Estadio.objects.all()
        }
        return render(request, self.template_name, self.context)


#////////////////////////////////////////////////////////////////////////////////
#CONSULTAS QUE SE ENCUENTRAN EN EL MENU DE DESPLIEGUE DESDE LA PAGINA WEB


#Ciudades con equipos:
class CityWithTeams(generic.View):
    template_name = "home/teams/citywithteams.html"
    context = {}

    def get(self, request):
        self.context = {
            "ciudades": Ciudad.objects.filter(equipo__isnull=False).distinct()
        }
        return render(request, self.template_name, self.context)


#Que equipo juega en que estadio:
class TeamStadium(generic.View):
    template_name = "home/teams/teamstadium.html"
    context = {}

    def get(self, request):
        self.context = {
            "teams": Team.objects.select_related('stadium')
        }
        return render(request, self.template_name, self.context)


#Que equipos tienen una ciudad:
class CityTeams(generic.View):
    template_name = "home/teams/cityteams.html"
    context = {}

    def get(self, request):
        self.context = {
            "ciudades": Ciudad.objects.exclude(equipo__isnull=True).distinct()
        }
        return render(request, self.template_name, self.context)