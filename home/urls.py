from django.urls import path
from home import views
from .views import (
    CityWithTeams, TeamStadium,
    CityTeams,
)

app_name = "home"

urlpatterns = [
   path('', views.Index.as_view(), name='index'),

   path('equipo/', views.TeamIndex.as_view(), name='equipoindex'),
   path('equipo/', views.EquipoIndex.as_view(), name="teamindex"),
   path('equipo/<int:pk>/', views.DetailTeam.as_view(), name='equipo_detail'),
   path('equipo/new/', views.NewTeam.as_view(), name='equipo_new'),
   path('equipo/<int:pk>/edit/', views.UpdateTeam.as_view(), name='equipo_edit'),
   path('equipo/<int:pk>/delete/', views.DeleteTeam.as_view(), name='equipo_delete'),

   path('estadio/', views.StadiumIndex.as_view(), name='estadioindex'),
   path('estadio/<int:pk>/', views.DetailEstadio.as_view(), name='estadio_detail'),
   path('estadio/new/', views.NewEstadio.as_view(), name='estadio_new'),
   path('estadio/<int:pk>/edit/', views.UpdateEstadio.as_view(), name='estadio_edit'),
   path('estadio/<int:pk>/delete/', views.DeleteEstadio.as_view(), name='estadio_delete'),

   path('ciudad/', views.CityIndex.as_view(), name='ciudadindex'),
   path('ciudad/<int:pk>/', views.DetailCiudad.as_view(), name='ciudad_detail'),
   path('ciudad/new/', views.NewCiudad.as_view(), name='ciudad_new'),
   path('ciudad/<int:pk>/edit/', views.UpdateCiudad.as_view(), name='ciudad_edit'),
   path('ciudad/<int:pk>/delete/', views.DeleteCiudad.as_view(), name='ciudad_delete'),



   path('equipo/citywithteams/', CityWithTeams.as_view(), name='cityWithTeams'),
   path('equipo/teamstadium/', TeamStadium.as_view(), name='teamsStadium'),
   path('equipo/cityteams/', CityTeams.as_view(), name='cityTeams'),
]
